import random
from tkinter import *
from tkinter.messagebox import showwarning


loginy = ["admin", "jasiek"]
passwords = ["admin", "qwerty"]

login_exist = False
password_is_right = False

window = Tk()
window.title('Logowanie do aplikacji sterującej')
window.geometry("800x350")

def ragequit():
    window.destroy()

def logging(login, password):
    global login_exist
    global password_is_right

    for data in loginy:
        if login == data:
            login_exist = True

    if login_exist == True:
        login_exist = False
        for data in loginy:
            if login == data:
                login_exist = True
        if login_exist == True:
            if (passwords[loginy.index(login)] == password):
                password_is_right = True

    xyz=0
    empty ="Nie podano żadnych danych"
    if(len(login) > 0):

        if((login_exist == True) and (password_is_right == True)):               
            tekst = f'Witaj użytkowniku {login}'
            login_label.pack_forget()
            password_label.pack_forget()
            login_button.pack_forget()
            login_entry.pack_forget()
            password_entry.pack_forget()
            return tekst, login

        elif((login_exist == True) and (password_is_right == False)):           
            tekst = f'Podane hasło jest niepoprawne'
            return tekst, xyz
        elif(login_exist == False):         
            tekst = f'Podana nazwa użytkownika jest niepoprawna '
            return tekst, xyz

    return empty, xyz

def check():
    feedback, received_data = logging(login_entry.get(),password_entry.get())
    info_label.config(text=feedback)
    if(received_data != 0):
        button_1to2.config(state="normal")



login_button = Button(window, text = "Wprowadź dane logowania", command=check,font=("Calibri", 12))
login_button.config(activebackground='#33CCFF')
login_button.pack(pady=10)

login_label = Label(window, text="Login", font=("Arial", 10,'bold'))
login_label.pack(pady=1)

login_entry = Entry(window)
login_entry.pack(pady=10)

password_label = Label(window, text="Hasło", font=("Arial", 10,'bold'))
password_label.pack(pady=1)

password_entry = Entry(window, show="*")
password_entry.pack(pady=10)

info_label = Label(window, text="", font=("Arial", 15,'bold'))
info_label.pack(pady=10)


button_1to2 = Button(window, text = "Zaloguj", state=DISABLED, command=ragequit,font=("Calibri", 18), )
button_1to2.config(activebackground='#33CCFF')
button_1to2.pack(pady=10)

window.mainloop()

if((login_exist == True) and (password_is_right == True)):



    sill = Tk()
    sill.title('Sterowanie v1')
    sill.geometry("1100x700")

    def ragequit2():
        sill.destroy()
        

    def sesja(remaining):
        if(remaining < 3600) and (int(remaining % 60) < 10):
            time_label.config(text=f' Czas sesji: {int((remaining % 3600) / 60)}:0{int(remaining % 60)}')
        elif(remaining < 3600):
            time_label.config(text=f' Czas sesji: {int((remaining % 3600) / 60)}:{int(remaining % 60)}')
        elif (int(remaining % 60) < 10) and (int((remaining % 3600) / 60)< 10):
            time_label.config(text=f' Czas sesji: {int(remaining / 3600)}:0{int((remaining % 3600) / 60)}:0{int(remaining % 60)}')
        elif (int(remaining % 60) < 10):
            time_label.config(text=f' Czas sesji: {int(remaining / 3600)}:{int((remaining % 3600) / 60)}:0{int(remaining % 60)}')
        elif (int((remaining % 3600) / 60)< 10):
            time_label.config(text=f' Czas sesji: {int(remaining / 3600)}:0{int((remaining % 3600) / 60)}:{int(remaining % 60)}')
        else:
            time_label.config(text=f' Czas sesji: {int(remaining / 3600)}:{int((remaining % 3600) / 60)}:{int(remaining % 60)}')
        remaining = remaining + 1
            
        sill.after(1000, lambda: sesja(remaining))


    def awake(period, count, init, kod, done, written):

        if(period == 60):
            # print("inicjacja" + str(count))
            init = random.randint(1,100)


        if(init > 90):
            if(period == 60):
                kod = random.randint(1000,9999)
                present_label_content.config(text = f'Wpisz kod: {kod}')
                present_label_time.config(text = f'Pozostało {count} sekund')
                present_entry.delete(0, END)
                present_frame.place(x=50,y=50)

            input = present_entry.get()

            if (str(kod) == input) and (written == False):
                present_label_content.config(text = f'Sesja przedłużona')
                present_label_time.config(text = f'')
                done = True
                written = True
                count = 2
                
                
            if(written == False):
                present_label_time.config(text = f'Pozostało {count} sekund')


        period -= 1
        count -= 1

        if(init > 90):
            if((count < 0) and (done==False) and (init > 50)):
                print("koniec")
                ragequit2()

            elif (count < 0) and (done==True):

                count = 30
                period = 60
                done = False
                written = False
                present_frame.place(x=3000,y=3000)


        if (init <= 90 and (count < 0)):
                count = 30
                period = 60
                done = False
                written = False


        if(period == 0):
            period = 60
            init = 0

        sill.after(1000, lambda: awake(period,count,init,kod, done, written))


    def low_speed(period, init):
        if(period == 10):
            init = random.randint(1,100)

        if(init > 10):
            if(period == 5):
                if((power_scale.get() > 0 ) and speed_scale.get() == 0):
                    showwarning(title = "Zerowa prędkość linii", message="Ustaw niezerową prędkość linii")


        period -= 1

        if (period == 0):
            if((power_scale.get() > 0 ) and speed_scale.get() == 0 and (init>10)):
                power_scale.set(0)
                power(0)
            period = 10
            init = 0

        sill.after(1000, lambda: low_speed(period,init)) 



    def power(lever):

        if lever == "0":
            onoff_label.config(text = "Linia Wyłączona", fg = "red")
            speed_label.config(text = f"Prędkość linii: {0} m/s", fg = "#ff9900")
        if lever == "1":
            onoff_label.config(text = "Linia Uruchomiona", fg = "green")
            speed_label.config(text = f"Prędkość linii: {speed_scale.get() } m/s", fg = "green")

    def slide(velocity):
        if (power_scale.get() > 0):
            speed_label.config(text = f"Prędkość linii: {float(velocity)} m/s", fg = "green")


    def cooling():

        global temperatura

        if (power_scale.get() == 1):
            tendencja = (speed_scale.get() / 2) - (cool_scale.get() / 110)
        else:
            tendencja = -1 * (cool_scale.get() / 40)

        temperatura += tendencja 

        if(temperatura < 18):
            temperatura = 18.0

        if(temperatura > 45):
            temperatura = 45.0
            power_scale.set(0)
            speed_scale.set(0)


        if(int(temperatura) == 35 and (tendencja>0)):
            temperatura+=1
            showwarning(title = "Upał", message="Zrobiło się bardzo gorąco. \nUstaw klimatyzację na większą moc lub zmniejsz prędkość linii produkcyjnej.\n"
            +"Jeśli tego nie zrobisz linia zostanie wkróce wyłączona")
            

        temperature_label.config(text = f"Temperatura Otoczenia: {round(temperatura,2)}°"  )


        sill.after(1000, lambda: cooling()) 


    # Sprawdzanie Przytomności
    time_label = Label(sill, text = "", font=("Arial", 13,'bold'))
    time_label.pack()
    time_label.place(x=0, y=0)

    present_frame = LabelFrame(sill, text="Obecność", padx=50, pady=50)
    present_frame.pack()
    present_frame.place(x=10, y=10)

    present_label_content = Label(present_frame, text = "Wpisz kod: 0000", font=("Arial", 10,'bold'))
    present_label_content.pack()

    present_label_time = Label(present_frame, text = "Pozostało 00 sekund", font=("Arial", 10,'bold'))
    present_label_time.pack()


    present_entry = Entry(present_frame)
    present_entry.pack()
    present_entry.place(x=5, y=50)


    # Menu Parametrów

    onoff_label = Label(sill, text = "Linia Wyłączona", font=("Arial", 50,'bold'), fg="red")
    onoff_label.pack()
    onoff_label.place(x=400, y=0)

    speed_label = Label(sill, text = "Prędkość linii: 0.0 m/s", font=("Arial", 20,'bold'), fg="red")
    speed_label.pack()
    speed_label.place(x=400, y=100)

    temperatura = 25.0

    temperature_label = Label(sill, text = f"Temperatura Otoczenia: {temperatura}°", font=("Arial", 20,'bold'), fg="#33cccc")
    temperature_label.pack()
    temperature_label.place(x=400, y=140)






    # Ustawienia linii


    power_label = Label(sill, text = "Włącznik linii produkcyjnej", font=("Arial", 11,'bold'))
    power_label.pack()
    power_label.place(x=400, y=300)

    power_scale = Scale(sill, from_ = 0, to=1, orient= HORIZONTAL, command = power)
    power_scale.pack()
    power_scale.place(x=400, y=330)


    speedset_label = Label(sill, text = "Prędkość linii produkcyjnej", font=("Arial", 11,'bold'))
    speedset_label.pack()
    speedset_label.place(x=650, y=300)

    speed_scale = Scale(sill, from_ = 0, to=2, orient= HORIZONTAL,resolution = 0.1, command = slide)
    speed_scale.pack()
    speed_scale.place(x=650, y=330)

    cool_label = Label(sill, text = "Klimatyzacja", font=("Arial", 11,'bold'))
    cool_label.pack()
    cool_label.place(x=400, y=500)

    cool_scale = Scale(sill, from_ = 0, to=100, orient= HORIZONTAL)
    cool_scale.pack()
    cool_scale.place(x=400, y=530)






    present_frame.place(x=3000,y=3000)
    awake(60,30,0,99999, False, False)
    sesja(0)
    low_speed(10,0)
    cooling()

    sill.mainloop()