## Readme - Logowanie do aplikacji sterującej

Ten kod to program logowania do aplikacji sterującej linią produkcyjną. Został napisany w języku Python przy użyciu modułów Tkinter i Random. Program zapewnia interfejs graficzny do logowania oraz sterowania parametrami linii produkcyjnej. 

### Funkcje programu:

1. Logowanie: Program zawiera okno logowania, w którym użytkownik musi wprowadzić poprawną nazwę użytkownika i hasło. Jeśli dane logowania są poprawne, użytkownik uzyskuje dostęp do okna sterowania linii produkcyjnej. W przeciwnym razie zostanie wyświetlony odpowiedni komunikat o błędzie.

2. Okno sterowania linii produkcyjnej: Po poprawnym zalogowaniu program otwiera nowe okno, w którym użytkownik może sterować różnymi parametrami linii produkcyjnej. Należą do nich:
   - Testy sprawdzające przytomność operatora: Losowo uruchamiane testy, w których operator musi wpisać czterocyfrowy kod w ciągu 30 sekund. Jeśli kod zostanie wprowadzony poprawnie, test zniknie. W przeciwnym razie linia produkcyjna zostanie zatrzymana, a użytkownik zostanie wylogowany.
   - Sterowanie prędkością linii produkcyjnej: Użytkownik może regulować prędkość linii przy pomocy suwaka (slidera).
   - Sterowanie klimatyzacją: Użytkownik może zmieniać natężenie klimatyzacji przy pomocy suwaka.
   - Monitorowanie temperatury otoczenia: Program wyświetla aktualną temperaturę otoczenia i informuje operatora o ewentualnym przekroczeniu dopuszczalnej wartości.

### Założenia programu:

- Program składa się z dwóch okienek: okna logowania i okna sterowania linii produkcyjnej.
- Użytkownik musi poprawnie zalogować się, aby uzyskać dostęp do okna sterowania.
- Okno sterowania wyświetla czas trwania sesji oraz losowo uruchamiane testy przytomności operatora.
- Użytkownik może regulować prędkość linii produkcyjnej i natężenie klimatyzacji.
- Jeśli temperatura otoczenia przekroczy 35 stopni, program wyświetli komunikat i zaleci odpowiednie działania.
- Jeśli temperatura przekroczy 45 stopni, linia produkcyjna zostanie wyłączona, aż temperatura spadnie do akceptowalnego poziomu.

### Implementacja:

Program wykorzystuje moduł Tkinter do tworzenia interfejsu graficznego. Okna i widżety są dynamicznie aktualizowane przy pomocy funkcji wywoływanych w głównej pętli programu Tkinter. Do generowania losowych wydarzeń i kodów używany jest moduł Random.

